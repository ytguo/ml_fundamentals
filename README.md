# README #

### Introduction ###
Collection of mini-projects that leverage basic machine learning alogrithms for data analysis. Projects are implemented in Python or R with Jupyter Notebooks and organized into different machine learning topics. For easy viewing of analysis and results, use the 'Ipython Notebook' viewer or reference the PDF versions.

### Topics ###
1. A/B Testing
2. Regularization
3. PCA
4. Classification (trees, SVM, logistic regression)
5. Clustering
6. Iterative methods (optimization)
7. Linear Regression
8. Neural Networks

### Setup ###
1. Install [Miniconda](https://docs.conda.io/en/latest/miniconda.html)
2. Download project files
3. Run command: ``` jupyter notebook```
